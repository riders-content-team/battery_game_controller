#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from std_srvs.srv import Trigger, TriggerResponse
import time
import os
import requests

class GameController:

    def __init__(self, robot_name):

        self.robot_status = "start"
        self.metrics = {
            'Power': '0',
            'Remaining Cables (red)': '0',
            'Remaining Cogs (blue)': '0',
            'Remaining PCBs (green)': '0',
            'Status': 'Running',
        }

        self.power = 3
        self.remaining_cable = 1
        self.remaining_cog = 3
        self.remaining_pcb = 3

        self.collected_battery = 0

        self.robot_name = robot_name

        rospy.init_node("game_controller")

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)
        self.publish_metrics()
        self.achieve(27)

        rospy.Subscriber("collision", String, self.contact_callback)
        rospy.Subscriber("robot_events", String, self.event_callback)

        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        while ~rospy.is_shutdown():
            time.sleep(0.1)
            self.publish_metrics()

            if self.remaining_cable == 0 and self.remaining_pcb == 0:
                self.achieve(20)
            if self.remaining_cable == 0 and self.remaining_pcb == 0 and self.remaining_cog == 0:
                self.achieve(25)
            if self.remaining_cable == 0 and self.remaining_pcb == 0 and self.remaining_cog == 0 and self.collected_battery == 9:
                self.achieve(26)
                self.robot_status = "stop"
            if self.robot_status == "stop":
                break

    def handle_robot(self,req):
        if self.robot_status == "start":
            self.robot_status = "run"
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "run":
            time.sleep(0.1)

            if self.power <= 0:
                self.metrics['status'] = "ran_out_of_power"
                self.robot_status = "stop"

                return TriggerResponse(
                    success = True,
                    message = self.robot_status
                )

            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

    def publish_metrics(self):
        self.metrics['Power'] = str(self.power)
        self.metrics['Remaining Cables (red)'] = str(self.remaining_cable)
        self.metrics['Remaining Cogs (blue)'] = str(self.remaining_cog)
        self.metrics['Remaining PCBs (green)'] = str(self.remaining_pcb)
        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def event_callback(self, data):
        if data.data is not 'init':
            self.power -= 1

    def contact_callback(self, data):
        if "battery" in data.data:
            self.power += 3
            self.collected_battery += 1
            self.achieve(24)
        elif "cog" in data.data:
            self.remaining_cog -= 1
            self.achieve(22)
        elif "cable" in data.data:
            self.remaining_cable -= 1
            self.achieve(21)
        elif "pcb" in data.data:
            self.remaining_pcb -= 1
            self.achieve(23)

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })

if __name__ == '__main__':
    # Name of robot
    controller = GameController("husky")
